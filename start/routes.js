'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'You can access /api/v1/todos for our example API' }
})

Route.group(()=> {
  Route.get('todos', 'TodoController.index')
  Route.get('todo/:id', 'TodoController.detail')
  Route.post('todo', 'TodoController.store')  
  Route.patch('todo/:id', 'TodoController.update')
  Route.delete('todo/:id', 'TodoController.delete')  
}).prefix('api/v1')

