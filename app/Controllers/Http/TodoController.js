'use strict'

const Todo = use('App/Models/Todo')

class TodoController {
  
  async index ({ request }) {
    return await Todo.query().orderBy('id', 'desc').fetch()
  }

  async store ({ request }) {
    const todo = await Todo.create(request.all())
    
    return todo
  }

  async detail ({ params }) {
    return await Todo.find(params.id)
  }

  async update ({ request, params }) {
    const todo = await Todo.query().where('id', params.id).update(request.all())

    return todo
  }

}

module.exports = TodoController
