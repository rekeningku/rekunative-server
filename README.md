# RekuNative Server [Example]

This is a repository for RekuNative server. You can use this as reference about list of API that can be consumed for frontend.

## Installation

Make sure you had NodeJS installed on your system, then:

- Clone this repository
```
$ git clone https://github.com/Rekeningku/rekunative-server
```
- Install AdonisJS (We use AdonisJS as Nodejs Framework)
```
$ npm i -g @adonisjs/cli
```
- Install Dependencies
```
$ npm install
```
- Open project and rename .env.example to be .env, and fill your setting (using our default setting will make u less confuse LOL)
- Install MySQL (we use MySQL for the database)
- Create database named "rekunative-server" or you can named it whatever you want, according to your .env setting (default using root as username, and blank password)
- Run Migration
```
adonis migration:run
```
- Run Seeder (for data example)
```
adonis seed
```
- Run your server app
```
adonis serve --dev
```

## List of Availables ENDPOINT

- GET    /api/v1/todos         List of todos
- GET    /api/v1/todo/:id      Todo detail
- POST   /api/v1/todo          Create a todo
- PATCH  /api/v1/todo/:id      Update a todo
- DELETE /api/v1/todo/:id      Delete a todo  
